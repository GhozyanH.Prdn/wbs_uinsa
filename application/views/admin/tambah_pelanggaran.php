<!DOCTYPE html>
<html>
  <?php $this->load->view('admin/head') ?>
<body class="hold-transition skin-blue sidebar-mini">
  <?php
    $tgl = date("Y-m-d");
  ?>
<div class="wrapper">

  <?php $this->load->view('admin/header') ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php $this->load->view('admin/leftbar') ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard admin
        <small>Whistle Blowing System</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Main Navigation</li>
      </ol>
    </section>

    <section class="content-header">
      <div class="row">
        <!-- left column -->
        <div class="col-md-10">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h1 class="box-title">BUAT JENIS PELANGGARAN</h1>
            </div>
            <!-- /.box-header -->
            <!-- form start -->

            <form <?php echo form_open_multipart('admin/Tambah_pelanggaran/upload')?>  
               
              
              <div class="box-body">
                 <div class="form-group">
                  <label for="exampleInputEmail1">Kode Pelanggaran</label>
                  <div class="input-group date">
                  <div class="input-group-addon">
                  </div>
                  <input type="text" name="kode_pelanggaran" class="form-control" id="kode_pelanggaran" placeholder="Kode Laporan" value="<?= $kode_pelanggaran; ?>" required="" readonly="">
                </div>
                <div class="form-group">
                <label>Tanggal Ditambahkan:</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                  </div>
                  <input type="date" name="tgl" class="form-control pull-right" id="tgl" value="<?= $tgl; ?>" required="" readonly="">
                </div>
              </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Nama Pelanggaran</label>
                  <input type="text" name="nama_pelanggaran" class="form-control" id="nama_pelanggaran" placeholder="Nama Pelanggaran" required="">
                </div>
                
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
      <!-- /.row -->
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

    <?php $this->load->view('admin/footer') ?>
</body>
</html>
