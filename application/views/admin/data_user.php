<!DOCTYPE html>
<html>
  <?php $this->load->view('admin/head') ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('admin/header') ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php $this->load->view('admin/leftbar') ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard Admin
        <small>Whistle Blowing System</small>

      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Main Navigation</li>



      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data User</h3>
              <div class="box-titler">
               <a href="tambah" class="btn btn-primary" style="text-decoration:none" ><strong>+</strong> Tambah Data</a>
               <!-- Garis Bawah Sudah Hilang-->
             </div >
            </div>
            <!-- /.box-header -->
            <div class="box-body">
               <table id="example2" class="table table-bordered table-hover">
                <thead>
        <tr>
            <th>No</th>
            <th>NIP Pengawas</th>
            <th>Nama Pengawas</th>
            <th>Jabatan</th>
            <th>Level</th>
            <th>Options</th>
        </tr>
      </thead>
      <tbody>
        <?php
    $no = 1;
    foreach($hasil as $r){
    ?>
    <tr>
      <td><?php echo $no++ ?></td>
      <td><?php echo $r['nip'] ?></td>
      <td><?php echo $r['nama']?></td>
      <td><?php echo $r['jabatan']?></td>
      <td><?php echo $r['level']?></td>
      <td>
      <a href="" class="btn btn-info btn-xs" >Ubah</a>
      <a href="" class="btn btn-danger btn-xs" >Hapus</a>
  </td>
  </tr>
   <?php } ?>
      </tbody>
    </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php $this->load->view('admin/footer') ?>
</body>
</html>
