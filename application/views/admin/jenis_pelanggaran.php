<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/head') ?>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Laporan Masuk</title>

	<link href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/datatables/css/dataTables.bootstrap.css')?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')?>" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.css');?>">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php $this->load->view('admin/header') ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php $this->load->view('admin/leftbar') ?>
 <div class="content-wrapper">
	<!-- Container -->

  <section class="content-header">
      <h1>
        Dashboard Admin
        <small>Whistle Blowing System</small>

      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Main Navigation</li>



      </ol>
    </section>

	<!-- Container -->
	<div class="">
    <h2 class="text-muted"></h2>
		<div class="panel panel-info">
      <div class="panel-heading">
      <div class="float-right"><a href="tambah_pelanggaran" class="btn btn-primary" ><span class="fa fa-plus"></span> Add New</a></div>
      </div>
			<div class="panel-body">
				<table id="table_id" class="table table-striped table-hover table-condesed" cellpadding="0" cellspacing="0">
					<thead>
						<th>#</th>
						<!-- <th> ID MAHASISWA</th> -->
						<th>KODE PELANGGARAN</th>
						<th>NAMA PELANGGARAN</th>
						<th>TANGGAL MENAMBAHKAN</th>
						<th>Option</th>
					</thead>
					<tbody>
						<?php
            $no = 0;
            foreach ($jenis_pelanggaran as $book) { $no++; ?>
							<tr>
								<td><?php echo $no;?></td>
								<td><?php echo $book->kode_pelanggaran;?></td>
								<td><?php echo $book->nama_pelanggaran;?></td>
								<td><?php echo $book->tgl;?></td>
								<td>
									<button class="btn btn-sm btn-default" onclick="edit_book(<?php echo $book->id_pelanggaran;?>)"><i class="glyphicon glyphicon-edit"></i></button>
									<button class="btn btn-sm btn-danger" onclick="delete_book(<?php echo $book->id_pelanggaran;?>)"><i class="glyphicon glyphicon-trash"></i></button>
								</td>
							</tr>
							<pre>
							<?php echo print_r($book);?>
						</pre>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
    </div>
    </div>
    <?php $this->load->view('admin/footer') ?>

	</div><!-- end of conatiner -->

	<?php $this->load->view('admin/modal1');?>

	<!-- <script src="<?php echo base_url('assets/jquery/jquery-2.1.4.min.js')?>"></script> -->
	<!-- <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js')?>"></script> -->
	<script src="<?php echo base_url('assets/datatables/js/jquery.dataTables.min.js')?>"></script>
	<script src="<?php echo base_url('assets/datatables/js/dataTables.bootstrap.js')?>"></script>
	<script src="<?php echo base_url('assets/bootstrap-datepicker/js/bootstrap-datepicker.min.js')?>"></script>

	<script type="text/javascript">
		$(document).ready(function () {
			$('#table_id').DataTable({
				responsive : true
			});
      //datepicker
      $('.datepicker').datepicker({
          autoclose: true,
          format: "yyyy",
          viewMode : "years",
          minViewMode : "years"
      });
		});
		var save_method; //for save method string
    var table;

    function add_book()
    {
      save_method = 'add';
      $('#form')[0].reset(); // reset form on modals
      $('#modal_form').modal('show'); // show bootstrap modal
    //$('.modal-title').text('Add Person'); // Set Title to Bootstrap modal title
    }

    function edit_book(id_pelanggaran)
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals

      //Ajax Load data from ajax
      $.ajax({
        url : "<?php echo site_url('admin/jenis_pelanggaran/ajax_edit/')?>/" + id_pelanggaran,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {

            $('[name="id_pelanggaran"]').val(data.id_pelanggaran);
            $('[name="kode_pelanggaran"]').val(data.kode_pelanggaran);
            $('[name="nama_pelanggaran"]').val(data.nama_pelanggaran);
            $('[name="tgl"]').val(data.tgl);

            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Ubah Nama Pelangaran'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
      });
    }

    function save()
    {
      var url;
      if(save_method == 'add')
      {
          url = "<?php echo site_url('admin/jenis_pelanggaran/book_add')?>";
      }
      else
      {
        url = "<?php echo site_url('admin/jenis_pelanggaran/book_update')?>";
      }

       // ajax adding data to database
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
               //if success close modal and reload ajax table
               $('#modal_form').modal('hide');
              location.reload();// for reload a page
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
    }

    function delete_book(id_pelanggaran)
    {
      if(confirm('Are you sure delete this data?'))
      {
        // ajax delete data from database
          $.ajax({
            url : "<?php echo site_url('admin/jenis_pelanggaran/book_delete')?>/"+id_pelanggaran,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {

               location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });

      }
    }
	</script>

</body>

</html>
