<!DOCTYPE html>
<html>
  <?php $this->load->view('admin/head') ?>

<head>
  <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Autocomplete</title>
  <link rel="stylesheet" href="<?php echo base_url().'assets/css/jquery-ui1.css'?>">
</head>

<body class="hold-transition skin-blue sidebar-mini">
  <?php
    $tgl_dibuat = date("Y-m-d");
  ?>
<div class="wrapper">

  <?php $this->load->view('admin/header') ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php $this->load->view('admin/leftbar') ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard admin
        <small>Whistle Blowing System</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Main Navigation</li>
      </ol>
    </section>

    <section class="content-header">
      <div class="row">
        <!-- left column -->
        <div class="col-md-10">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h1 class="box-title">BUAT UNDANGAN PEMERIKSAAN</h1>
            </div>
            <!-- /.box-header -->
            <!-- form start -->

            <form <?php echo form_open_multipart('admin/buat_undangan/upload')?>  
               
              
              <div class="box-body">
               <input type="text" name="kode_undangan" class="form-control" id="kode_undangan" placeholder="Kode Undangan" value="<?= $kode_undangan; ?>" required="" readonly="">
               <input type="hidden" name="tgl_dibuat" class="form-control pull-right" id="tgl_dibuat" value="<?= $tgl_dibuat; ?>" required="" readonly="">

               <div class="form-group">
                <label>Pilih Kode Laporan</label>
                <input type="text" name="kode_lapor" class="form-control" id="title" placeholder="Pilih Kode Laporan" >
                </div>
                 <div class="form-group">
                <label>Terlapor</label>
               <input type="text" name="nama" class="form-control" placeholder="" readonly="" ></input>
                </div>

               <div class="form-group">
                <label>Tanggal Undangan:</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="date" name="tgl_undangan" class="form-control" id="datepicker" required="">
                </div>
              </div>

                 <div class="form-group">
                <label>Waktu Undangan (WIB)</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" name="pukul" class="form-control" id="datepicker" required="">
                </div>
              </div>

               
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
      <!-- /.row -->
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

    <?php $this->load->view('admin/footer') ?>
</body>
<!--  <script src="<?php echo base_url().'assets/assets/js/jquery-3.3.1.js'?>" type="text/javascript"></script> -->
  <script src="<?php echo base_url().'assets/assets/js/bootstrap.js'?>" type="text/javascript"></script>
  <script src="<?php echo base_url().'assets/assets/js/jquery-ui.js'?>" type="text/javascript"></script>
  <script type="text/javascript">
    $(document).ready(function(){

        $('#title').autocomplete({
                source: "<?php echo site_url('admin/buat_undangan/get_autocomplete');?>",
     
                select: function (event, ui) {
                    $('[name="kode_lapor"]').val(ui.item.label); 
                    $('[name="nama"]').val(ui.item.description); 
                }
            });

    });
  </script>
</html>



