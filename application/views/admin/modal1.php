<div class="modal fade" id="modal_form" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" aria-hidden="true" data-dismiss="modal" aria-Label="Close">&times;</button>
				<h3 class="modal-title">Ubah Nama Pelanggaran</h3>
			</div>
			<div class="modal-body form">
				<form action="#" id="form" class="form-horizontal">
					<input type="hidden" name="id_pelanggaran">
          <div class="form-group">
              <label class="control-label col-md-3">Kode Pelanggaran</label>
              <div class="col-md-9">
                <input name="kode_pelanggaran" placeholder="Title Books" class="form-control" type="text" readonly>
                <span class="help-block"></span>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Tanggal Edit</label>
              <div class="col-md-9">
                <input name="tgl" placeholder="Title Books" class="form-control" type="date" readonly>
                <span class="help-block"></span>
              </div>
            </div>
          <div class="form-group">
              <label class="control-label col-md-3">Nama Pelanggaran</label>
              <div class="col-md-9">
                <input name="nama_pelanggaran" placeholder="Title Books" class="form-control" type="text">
                <span class="help-block"></span>
              </div>
            </div>
					
            
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
				<button type="reset" class="btn btn-warning" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>