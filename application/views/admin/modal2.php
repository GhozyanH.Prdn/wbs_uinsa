<div class="modal fade" id="modal_form" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" aria-hidden="true" data-dismiss="modal" aria-Label="Close">&times;</button>
				<h3 class="modal-title">Ubah Status Pengaduan</h3>
			</div>
			<div class="modal-body form">
				<form action="#" id="form" class="form-horizontal">
        <input type="hidden" name="id_laporan">
          <input type="hidden" name="id">
          <input type="hidden" name="kode_lapor">
          <input type="hidden" name="tgl_lapor">
          <input type="hidden" name="nama">
          <input type="hidden" name="nip">
          <input type="hidden" name="pelanggaran">
          <input type="hidden" name="tempat_kejadian">
          <input type="hidden" name="tanggal">
          <input type="hidden" name="uraian">
          <input type="hidden" name="bukti">
          <input type="hidden" name="status">
          <input type="hidden" name="tgl_undangan">
					<div class="form-body">
             <div class="form-group">
                  <label class="control-label col-md-3" >STATUS VERIVIKASI</label>
                  <div class="col-md-9">
                  <select name="status_verivikasi" class="form-control">
                    <option>ON PROGRESS</option>
                    <option>SELESAI</option>
                  </select>
                  <span class="help-block"></span>
                </div>
                </div>
            
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
				<button type="reset" class="btn btn-warning" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>