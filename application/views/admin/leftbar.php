<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?php echo base_url('assets/template/back/dist') ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p><?php echo $this->session->userdata('ses_nama');?></p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    <!-- search form -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
        <span class="input-group-btn">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                <i class="fa fa-search"></i>
              </button>
            </span>
      </div>
    </form>
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">MAIN NAVIGATION</li>
       <li>
        <a href="home">
          <i class="fa fa-tachometer"></i> <span>Dashboard</span>
        </a>
      </li>
       <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Laporan Masuk</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="laporan_masuk"><i class="fa fa-circle-o"></i> Laporan Masuk</a></li>
            <li><a href="laporan_accepted"><i class="fa fa-circle-o"></i> Laporan Ter Verifikasi</a></li>
          </ul>
        </li>
       <li>
        <a href="person">
          <i class="fa fa-pencil-square-o"></i> <span>Buat & Cetak Undangan</span>
        </a>
      </li>
       <li>
        <a href="BAP">
          <i class="fa fa-print"></i> <span>Cetak BAP</span>
        </a>
      </li>
      <li>
        <a href="jenis_pelanggaran">
          <i class="fa fa-list-alt"></i> <span>Jenis Pelanggaran</span>
        </a>
      </li>
      <li>
        <a href="laporan_selesai">
          <i class="fa fa-folder-o"></i> <span>Laporan Selesai</span>
        </a>
      </li>
  </section>
  <!-- /.sidebar -->
</aside>
