<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Conference</title>

    <!-- css -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/main.css">
</head>
<body data-spy="scroll" data-target="#site-nav">
    <nav id="site-nav" class="navbar navbar-fixed-top navbar-custom">
        <div class="container">
            <div class="navbar-header">

                <!-- logo -->
                <div class="site-branding">
                    <a class="logo" href="index.html">
                        
                        <!-- logo image  -->
                        <img src="<?php echo base_url(); ?>assets/images/logo.png" alt="Logo">

                        SPI  UINSA
                    </a>
                </div>

                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-items" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

            </div><!-- /.navbar-header -->

             <div class="collapse navbar-collapse" id="navbar-items">
                <ul class="nav navbar-nav navbar-right">

                    <!-- navigation menu -->
                    <li class="active"><a href="Home">HOME</a></li>
                    <li class="dropdown submenu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">+ INFORMASI</a>
                            <ul class="dropdown-menu other_dropdwn">
                                <li><a href="<?php echo base_url();?>index.php/tentang">Tentang WPS</a></li>
                                <li><a href="#">Alur Pengaduan</a></li>
                                <li><a href="<?php echo base_url();?>index.php/tatacara">Tata Cara Pengaduan</a></li>
                            </ul>
                    </li>
                    <li class="dropdown submenu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">+ BANTUAN</a>
                            <ul class="dropdown-menu other_dropdwn">
                                <li><a href="<?php echo base_url();?>index.php/Faqs">FAQs</a></li>
                                <li><a href="#">Hubungi Kami</a></li>
                            </ul>
                    </li>
                    <li class="dropdown submenu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">+  PENGADUAN</a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo base_url();?>index.php/warning">Tulis Pengaduan</a></li>
                                <li><a href="blog-2.html">Pantau Pengaduan</a></li>
                                <li><a href="blog-2.html">Grafik Pengaduan</a></li>
                            </ul>
                        </li>
                     <li><a href="<?php echo base_url();?>index.php/login">LOGIN</a></li>
                </ul>
            </div>
        </div><!-- /.container -->
    </nav>
       
    <section id="contribution" class="section bg-image-2 contribution">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="text-uppercase mt0 font-650">TENTANG WBS</h1>
                    <h2 >Berikut ini adalah penjelasan tentang aplikasi WBS <br> (Whistleblowing System)</h2>
                </div>
            </div>
        </div>
    </section>

    <section id="faq" class="section faq">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>WHISTLE BLOWING SYSTEM</h2>
                </div>
            </div>
             <p>Mekanisme penyampaian pengaduan dugaan tindak pidana tertentu yang telah terjadi atau akan terjadi yang melibatkan pegawai dan orang lain yang yang dilakukan dalam organisasi tempatnya bekerja, dimana pelapor bukan merupakan bagian dari pelaku kejahatan yang dilaporkannya.</p>

             <div class="row">
             <div class="col-md-12">
                    <h2>Kriteria Pengaduan</h2>
                </div>
            </div>
             <br>
                    Jika Anda melihat atau mengetahui dugaan Tindak Pidana Korupsi atau bentuk pelanggaran lainnya yang dilakukan pegawai SPI UINSA, silahkan melapor ke Inspektorat SPI UINSA. Jika laporan anda memenuhi syarat/kriteria, maka laporan Anda akan diproses lebih lanjut.

                    <ul>
                        <li>
                         <strong>WHAT</strong> yaitu apa perbuatan berindikasi Tindak Pidana Korupsi/pelanggaran yang diketahui.</li>
                        <li>
                        <strong>WHO</strong> yaitu siapa yang bertanggungjawab/terlibat dan terkait dalam perbuatan tersebut.</li>
                        <li>
                        <strong>WHERE</strong> yaitu dimana tempat terjadinya perbuatan tersebut dilakukan.</li>
                        <li>
                        <strong>WHEN</strong> yaitu kapan waktu perbuatan tersebut dilakukan.</li>
                         <li>
                        <strong>HOW</strong> yaitu Bagaimana cara perbuatan tersebut dilakukan (modus, cara, dan sebagainya).</li>
                        <li>
                        <strong>EVIDENCE (jika ada)</strong> yaitu Dilengkapi dengan bukti permulaan (data, dokumen, gambar dan rekaman) yang mendukung.</li>
                        <br>
                    </ul>



                <div class="row">
                <div class="col-md-12">
                    <h2>Jaminan Kerahasiaan dan Komitmen Kami</h2>
                </div>
            </div>
             <p>Anda tidak perlu khawatir terungkapnya identitas diri anda karena SPI UINSA akan MERAHASIAKAN & MELINDUNGI Identitas Anda sebagai whistleblower. SPI UINSA sangat menghargai informasi yang Anda laporkan. Fokus kami kepada materi informasi yang Anda Laporkan. Sebagai bentuk terimakasih kami terhadap laporan yang Anda kirim, kami berkomitmen untuk merespon laporan Anda selambat-lambatnya 7 (tujuh) hari kerja sejak laporan Anda dikirim.</p>

            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>




    <footer class="site-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="site-info">WHISTLE BLOWING SYSTEM INTERNAL UINSA <br>
                        SATUAN PENGAWAS INTERNAL 
                        <br>JL. A. Yani 117, Surabaya Jawa Timur, Indonesia, 60237
                        <br>Telp. +62 31 8410298 ext.141 <br>Fax. +62 31 8413300
                        <br> </p><img src="<?php echo base_url(); ?>assets/images/logo1.png" alt="Logo">





                    <ul class="social-block">
                        <li><a href=""><i class="ion-social-twitter"></i></a></li>
                        <li><a href=""><i class="ion-social-facebook"></i></a></li>
                        <li><a href=""><i class="ion-social-linkedin-outline"></i></a></li>
                        <li><a href=""><i class="ion-social-googleplus"></i></a></li>
                    </ul>
                        <div class="footer2">Hak cipta &copy; 2018 SATUAN PENGAWAS INTERNAL UINSA <br>- All Rights Reserved -</div>

</div>
                </div>
            </div>
        </div>
    </footer>

    <!-- script -->
    <script src="<?php echo base_url(); ?>assets/jquery/dist/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/smooth-scroll/dist/js/smooth-scroll.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/main.js"></script>
</body>
</html>