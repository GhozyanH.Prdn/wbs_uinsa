<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Whistleblowing System UINSA</title>

    <!-- css -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/main.css">
</head>
<body data-spy="scroll" data-target="#site-nav">
    <nav id="site-nav" class="navbar navbar-fixed-top navbar-custom">
        <div class="container">
            <div class="navbar-header">

                <!-- logo -->
                <div class="site-branding">
                    <a class="logo" href="index.html">
                        
                        <!-- logo image  -->
                        <img src="<?php echo base_url(); ?>assets/images/logo.png" alt="Logo">

                        SPI  UINSA
                    </a>
                </div>

                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-items" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

            </div><!-- /.navbar-header -->

             <div class="collapse navbar-collapse" id="navbar-items">
                <ul class="nav navbar-nav navbar-right">

                    <!-- navigation menu -->
                    <li class="active"><a href="Home">HOME</a></li>
                    <li class="dropdown submenu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">+ INFORMASI</a>
                            <ul class="dropdown-menu other_dropdwn">
                                <li><a href="<?php echo base_url();?>index.php/tentang">Tentang WPS</a></li>
                                <li><a href="#">Alur Pengaduan</a></li>
                                <li><a href="<?php echo base_url();?>index.php/tatacara">Tata Cara Pengaduan</a></li>
                            </ul>
                    </li>
                    <li class="dropdown submenu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">+ BANTUAN</a>
                            <ul class="dropdown-menu other_dropdwn">
                                <li><a href="<?php echo base_url();?>index.php/Faqs"">FAQs</a></li>
                                <li><a href="#">Hubungi Kami</a></li>
                            </ul>
                    </li>
                    <li class="dropdown submenu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">+  PENGADUAN</a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo base_url();?>index.php/warning">Tulis Pengaduan</a></li>
                                <li><a href="blog-2.html">Pantau Pengaduan</a></li>
                                <li><a href="blog-2.html">Grafik Pengaduan</a></li>
                            </ul>
                        </li>
                    <li><a href="<?php echo base_url();?>index.php/login">LOGIN</a></li>
                
                </ul>
            </div>
        </div><!-- /.container -->
    </nav>

    <header id="site-header" class="site-header valign-center"> 
        <div class="intro">
            <h2>بِسْــــــــــــــــــمِ اﷲِالرَّحْمَنِ اارَّحِيم</h2>
            <h2>SELAMAT DATANG DI</h2>
            <br>
            
            <h1>WHISTLE BLOWING SYSTEM <br> SATUAN PENGAWAS INTERNAL UINSA</h1>
            
            <p>"MARI BERSAMA-SAMA MENCIPTAKAN PEMERINTAHAN YANG JUJUR DAN BERSIH, <br>LAPORKAN SETIAP PELANGGARAN YANG TERJADI DI LINGKUNGAN KERJA"</p>
            <br>
            <br>
            <br>
            <br>
            <a class="btn btn-white" href="index.php/login">TULIS PENGADUAN</a>

        
        </div>
    </header>

       

    <section id="about" class="section about">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">

                    <h3 class="section-title">WHISTLEBLOWING SYSTEM</h3>

                    <p>mekanisme penyampaian pengaduan dugaan tindak pidana tertentu yang telah terjadi atau akan terjadi yang melibatkan pegawai dan orang lain yang yang dilakukan dalam organisasi tempatnya bekerja, dimana pelapor bukan merupakan bagian dari pelaku kejahatan yang dilaporkannya.</p>

                    <figure>
                        <img alt="" class="img-responsive" src="<?php echo base_url(); ?>assets/images/about-us1.jpeg">
                    </figure>

                </div><!-- /.col-sm-6 -->

                <div class="col-sm-6">

                    <h3 class="section-title">JAMINAN LAPORAN</h3>

                    <p> SPI akan Merahasiakan & Melindungi Identitas Anda sebagai whistleblower. SPI sangat menghargai informasi yang Anda laporkan. Sebagai bentuk terimakasih kami terhadap laporan yang Anda kirim, kami berkomitmen untuk merespon laporan Anda selambat-lambatnya 7 hari kerja sejak laporan dikirim.</p>

                    <figure>
                        <img alt="" class="img-responsive" src="<?php echo base_url(); ?>assets/images/about-us.jpg">
                    </figure>

                </div><!-- /.col-sm-6 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section>
     <!-- Professional Builde -->



    <section id="contribution" class="section bg-image-2 contribution">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="text-uppercase mt0 font-400">AYO BUAT PENGADUAN</h3>
                    
                    <p>Jika Anda melihat atau mengetahui dugaan Tindak Pidana Korupsi atau bentuk pelanggaran lainnya yang dilakukan oleh seluruh intansi di UINSA, silahkan melapor ke SPI. Jika laporan anda memenuhi syarat/kriteria, maka laporan Anda akan diproses lebih lanjut.</p>
                </div>
            </div>
        </div>
    </section>

    <section id="faq" class="section faq">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="section-title">KRITERIA PENGADUAN</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a class="faq-toggle collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">WHAT?</a>
                                </h4>
                            </div>

                            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <h3>WHAT</h3>
                                    <p> apa perbuatan berindikasi Tindak Pidana Korupsi/pelanggaran yang diketahui</p>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="faq-toggle collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">WHEN?</a>
                                </h4>
                            </div>

                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <h3>WHEN</h3>
                                    <p> kapan waktu perbuatan tersebut dilakukan</p>
                                </div>
                            </div>
                        </div>
  
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="faq-toggle collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree"> WHO ?</a>
                                </h4>
                            </div>

                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <h3>WHO</h3>
                                    <p> siapa yang bertanggungjawab/terlibat dan terkait dalam perbuatan tersebut</p>
                                </div>
                            </div>
                        </div>


                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingFive">
                                <h4 class="panel-title">
                                    <a class="faq-toggle collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive"> WHERE ?</a>
                                </h4>
                            </div>

                            <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                                <div class="panel-body">
                                    <h3>WHERE</h3>
                                    <p> dimana tempat terjadinya perbuatan tersebut dilakukan</p>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingFour">
                                <h4 class="panel-title">
                                    <a class="faq-toggle collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour"> HOW ?</a>
                                </h4>
                            </div>

                            <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                <div class="panel-body">
                                    <h3>HOW</h3>
                                    <p> Bagaimana cara perbuatan tersebut dilakukan (modus, cara, dan sebagainya)</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>


     <section id="facts" class="section bg-image-1 facts text-center">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">

                    <i class="ion-ios-calendar"></i>
                    <h3>PERIKSA LAPORAN ANDA</h3>
                
                </div>
                <div class="col-sm-3">

                    <i class="ion-ios-location"></i>
                    <h3>SILAHKAN LOGIN/REGISTRASI</h3>
                
                </div>
                <div class="col-sm-3">

                    <i class="ion-pricetags"></i>
                    <h3>ISI FORMULIR PENGADUAN</h3>
                
                </div>
                <div class="col-sm-3">
                
                    <i class="ion-speakerphone"></i>
                    <h3>PANTAU STATUS PENGADUAN</h3>
                
                </div>
            </div><!-- row -->
        </div><!-- container -->
    </section>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>

    <footer class="site-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="site-info">WHISTLE BLOWING SYSTEM INTERNAL UINSA <br>
                        SATUAN PENGAWAS INTERNAL 
                        <br>JL. A. Yani 117, Surabaya Jawa Timur, Indonesia, 60237
                        <br>Telp. +62 31 8410298 ext.141 <br>Fax. +62 31 8413300
                        <br> </p><img src="<?php echo base_url(); ?>assets/images/logo1.png" alt="Logo">





                    <ul class="social-block">
                        <li><a href=""><i class="ion-social-twitter"></i></a></li>
                        <li><a href=""><i class="ion-social-facebook"></i></a></li>
                        <li><a href=""><i class="ion-social-linkedin-outline"></i></a></li>
                        <li><a href=""><i class="ion-social-googleplus"></i></a></li>
                    </ul>
                        <div class="footer2">Hak cipta &copy; 2018 SATUAN PENGAWAS INTERNAL UINSA <br>- All Rights Reserved -</div>

</div>
                </div>
            </div>
        </div>
    </footer>

    <!-- script -->
    <script src="<?php echo base_url(); ?>assets/jquery/dist/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/smooth-scroll/dist/js/smooth-scroll.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/main.js"></script>
</body>
</html>