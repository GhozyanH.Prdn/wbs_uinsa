<div class="modal fade" id="modal_form" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" aria-hidden="true" data-dismiss="modal" aria-Label="Close">&times;</button>
				<h3 class="modal-title">Detail Pengaduan</h3>
			</div>
			<div class="modal-body form">
				<form action="#" id="form" class="form-horizontal">
          <input type="hidden" name="id_laporan">
          <input type="hidden" name="id">
          <input type="hidden" name="tempat_kejadian">
          <input type="hidden" name="tanggal">
          <input type="hidden" name="uraian">
          <input type="hidden" name="bukti">
          <input type="hidden" name="tgl_undangan">
           <input type="hidden" name="status_verivikasi">
            <div class="form-group">
                <label class="control-label col-md-3">Kode Lapor</label>
                <div class="col-md-9">
                  <input name="kode_lapor" placeholder="Title Books" class="form-control" type="text" readonly>
                  <span class="help-block"></span>
                </div>
              </div>
            <div class="form-group">
              <label class="control-label col-md-3">Tanggal Lapor</label>
              <div class="col-md-9">
                <input name="tgl_lapor" placeholder="Title Books" class="form-control" type="text" readonly>
                <span class="help-block"></span>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Nama Terlapor</label>
              <div class="col-md-9">
                <input name="nama" placeholder="Title Books" class="form-control" type="text" readonly>
                <span class="help-block"></span>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">NIP</label>
              <div class="col-md-9">
                <input name="nip" placeholder="Title Books" class="form-control" type="text" readonly>
                <span class="help-block"></span>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Jenis Pelanggaran</label>
              <div class="col-md-9">
                <input name="pelanggaran" placeholder="Title Books" class="form-control" type="text" readonly>
                <span class="help-block"></span>
              </div>
            </div>
            
            <div class="form-group">
              <label class="control-label col-md-3">Status Bukti</label>
              <div class="col-md-9">
                <input name="status" placeholder="Title Books" class="form-control" type="text" readonly>
                <span class="help-block"></span>
              </div>
            </div>
            
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="reset" class="btn btn-warning" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>