<!DOCTYPE html>
<html>
  <?php $this->load->view('admin/head') ?>
 <!-- Main content -->
      <section class="content">
        <div class="callout callout-danger">
          <h1> <center><strong>Warning !!</strong></center></h1>
          <h3><center> -- Maaf Anda Tidak Bisa Mengakses Halaman Ini -- </center></h3>
          <h3><center>Halaman Ini Telah Di Enkripsi<br> Silahkan Login Menurut Hak Akses Halaman Terlebih dahulu !!</center></h3>
           <div class=" callout-danger">
            <br>
             <div class="container">
               <center><a href="<?php echo base_url();?>index.php/login" class="btn btn-primary" style="text-decoration:none" >SILAHKAN LOGIN !!</a></center>
               <!-- Garis Bawah Sudah Hilang-->
             </div >
              </div>
        </div>

        <!-- /.box -->
      </section>
    <!-- /.content -->
  </div>
</body>
</html>
