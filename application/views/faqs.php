<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Conference</title>

    <!-- css -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/main.css">
</head>
<body data-spy="scroll" data-target="#site-nav">
    <nav id="site-nav" class="navbar navbar-fixed-top navbar-custom">
        <div class="container">
            <div class="navbar-header">

                <!-- logo -->
                <div class="site-branding">
                    <a class="logo" href="index.html">
                        
                        <!-- logo image  -->
                        <img src="<?php echo base_url(); ?>assets/images/logo.png" alt="Logo">

                        SPI  UINSA
                    </a>
                </div>

                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-items" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

            </div><!-- /.navbar-header -->

           <div class="collapse navbar-collapse" id="navbar-items">
                <ul class="nav navbar-nav navbar-right">

                    <!-- navigation menu -->
                    <li class="active"><a href="Home">HOME</a></li>
                    <li class="dropdown submenu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">+ INFORMASI</a>
                            <ul class="dropdown-menu other_dropdwn">
                                <li><a href="<?php echo base_url();?>index.php/tentang">Tentang WPS</a></li>
                                <li><a href="#">Alur Pengaduan</a></li>
                                <li><a href="<?php echo base_url();?>index.php/tatacara">TataCara Pengaduan</a></li>
                            </ul> 
                    </li>
                    <li class="dropdown submenu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">+ BANTUAN</a>
                            <ul class="dropdown-menu other_dropdwn">
                                <li><a href="<?php echo base_url();?>index.php/faqs">FAQs</a></li>
                                <li><a href="#">Hubungi Kami</a></li>
                            </ul>
                    </li>
                    <li class="dropdown submenu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">+  PENGADUAN</a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo base_url();?>index.php/warning">Tulis Pengaduan</a></li>
                                <li><a href="blog-2.html">Pantau Pengaduan</a></li>
                                <li><a href="blog-2.html">Grafik Pengaduan</a></li>
                            </ul>
                        </li>
                    <li><a href="<?php echo base_url();?>index.php/login">LOGIN</a></li>
                
                </ul>
            </div>
        </div><!-- /.container -->
    </nav>
       
    <section id="contribution" class="section bg-image-2 contribution">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="text-uppercase mt0 font-650">FREQUENTLY ASKED QUESTIONS</h1>
                    <h4 >Berikut ini adalah daftar pertanyaan yang sering ditanyakan terkait WBS SPI UINSA</h4>
                </div>
            </div>
        </div>
    </section>

    <section id="faq" class="section faq">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                   <div class="subtittle">
                        <h3>1. Apakah manfaat dari WBS?</h3>
                    </div>
                    Manfaat dari aplikasi WBS SPI UINSA adalah:<br> <br><ol>
                    <li>Fasilitas bagi Aparat Pengawas/Inspektorat untuk mendeteksi dini adanya pelanggaran di unit-unit kerja</li>
                    <li>Memberikan perlindungan pada pengadu</li>
                    <li>Fasilitas bagi pegawai dan masyarakat untuk mengadukan penyimpangan tanpa mengungkap identitas</li>
                    <li>Mendorong partisipasi pegawai/masyarakat dalam upaya pencegahan dan pemberantasan KKN</li>
                    <li>Mendorong terciptanya Pembangunan Zona Integritas</li>
                    <li>Mendorong peningkatan Reformasi Birokrasi</li>
                    </ol>
                    <br>
                    <br>


                    

                    <div class="subtittle">
                        <h3>2. Waktu Verivikasi Jawaban?</h3>
                    </div>
                    Sesuai dengan KMK 149 tahun 2011 jawaban/respon atas pengaduan yang disampaikan wajib diberikan dalam kurun waktu paling lambat 7 (tujuh) hari terhitung sejak pengaduan diterima.
                    <br>
                    <br>
                    <br>

                    <div class="subtittle">
                        <h3>3.  Apakah bentuk respon yang diberikan kepada pelapor atas pengaduan yang disampaikan?</h3>
                    </div>
                    Respon yang diberikan kepada pelapor berupa status/tindak lanjut pengaduan paling akhir sesuai dengan respon yang telah diberikan oleh pihak penerima pengaduan. Respon terkait dengan status/tindak lanjut pengaduan dapat dilihat dalam history pengaduan aplikasi WBS.
                    <br>
                    <br>
                    <br>
                    <br>
                   
                    <div class="subtittle">
                        <h3>4.  Apakah setiap melakukan pengaduan harus membuat dan register username baru?</h3>
                    </div>
                    Hal tersebut tidak perlu dilakukan. Satu username dapat melakukan pengaduan lebih dari satu. Setelah selesai membuat satu pengaduan, anda dapat membuat pengaduan terkait dengan dugaan pelanggaran dan/atau ketidakpuasan terhadap pelayanan yang diberikan lainnya dengan memilih "tambah pengaduan". Masing-masing pengaduan akan mendapatkan nomor register yang berbeda. 
                    <br>
                    <br>
                    <br>
                    <br>

                    <div class="subtittle">
                        <h3>5.  Apakah pengaduan yang saya berikan akan selalu mendapatkan respon?</h3>
                    </div>
                    Pengaduan yang anda berikan akan direspon dan tercantum dalam aplikasi WBS ini dan akan terupdate secara otomatis sesuai dengan respon yang telah diberikan oleh pihak penerima pengaduan. Untuk dapat melihat respon yang diberikan, anda harus login terlebih dahulu dengan username yang telah anda registrasikan di aplikasi ini dan anda dapat melihat status pengaduan dalam history pengaduan sesuai dengan nomor register pengaduan yang didapatkan. Sebagai catatan, pengaduan anda akan lebih mudah ditindaklanjuti apabila memenuhi unsur pengaduan.
                    <br>
                    <br>
                    <br>
                    <br>

                    <div class="subtittle">
                        <h3>6.  Saya sudah mengirimkan pengaduan namun di kemudian hari saya ingin merubah/menambahkan data terkait pengaduan yang saya lakukan, apa yang harus saya lakukan? Apakah harus membuat pengaduan baru?</h3>
                    </div>
                    Data yang sudah dilaporkan sebelumnya tidak dapat dilakukan perubahan. Karena data yang masuk langsung diseleksi oleh admin untuk proses verivikasi bukti pengaduan. Oleh karena itu jika ingin mengganti pengaduan hal yang harus dilakaukan adalah membuat pengaduan baru yang disertai oleh bukti yang kuat.
                    <br>
                    <br>
                    <br>
                    <br>
            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>




    <footer class="site-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="site-info">WHISTLE BLOWING SYSTEM INTERNAL UINSA <br>
                        SATUAN PENGAWAS INTERNAL 
                        <br>JL. A. Yani 117, Surabaya Jawa Timur, Indonesia, 60237
                        <br>Telp. +62 31 8410298 ext.141 <br>Fax. +62 31 8413300
                        <br> </p><img src="<?php echo base_url(); ?>assets/images/logo1.png" alt="Logo">





                    <ul class="social-block">
                        <li><a href=""><i class="ion-social-twitter"></i></a></li>
                        <li><a href=""><i class="ion-social-facebook"></i></a></li>
                        <li><a href=""><i class="ion-social-linkedin-outline"></i></a></li>
                        <li><a href=""><i class="ion-social-googleplus"></i></a></li>
                    </ul>
                        <div class="footer2">Hak cipta &copy; 2018 SATUAN PENGAWAS INTERNAL UINSA <br>- All Rights Reserved -</div>

</div>
                </div>
            </div>
        </div>
    </footer>

    <!-- script -->
    <script src="<?php echo base_url(); ?>assets/jquery/dist/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/smooth-scroll/dist/js/smooth-scroll.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/main.js"></script>
</body>
</html>