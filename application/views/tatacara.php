<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Conference</title>

    <!-- css -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/main.css">
</head>
<body data-spy="scroll" data-target="#site-nav">
    <nav id="site-nav" class="navbar navbar-fixed-top navbar-custom">
        <div class="container">
            <div class="navbar-header">

                <!-- logo -->
                <div class="site-branding">
                    <a class="logo" href="index.html">
                        
                        <!-- logo image  -->
                        <img src="<?php echo base_url(); ?>assets/images/logo.png" alt="Logo">

                        SPI  UINSA
                    </a>
                </div>

                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-items" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

            </div><!-- /.navbar-header -->

            <div class="collapse navbar-collapse" id="navbar-items">
                <ul class="nav navbar-nav navbar-right">

                    <!-- navigation menu -->
                    <li class="active"><a href="Home">HOME</a></li>
                    <li class="dropdown submenu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">+ INFORMASI</a>
                            <ul class="dropdown-menu other_dropdwn">
                                <li><a href="<?php echo base_url();?>index.php/tentang">Tentang WPS</a></li>
                                <li><a href="#">Alur Pengaduan</a></li>
                                <li><a href="<?php echo base_url();?>index.php/tatacara">Tata Cara Pengaduan</a></li>
                            </ul>
                    </li>
                    <li class="dropdown submenu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">+ BANTUAN</a>
                            <ul class="dropdown-menu other_dropdwn">
                                <li><a href="<?php echo base_url();?>index.php/Faqs">FAQs</a></li>
                                <li><a href="#">Hubungi Kami</a></li>
                            </ul>
                    </li>
                    <li class="dropdown submenu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">+  PENGADUAN</a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo base_url();?>index.php/warning">Tulis Pengaduan</a></li>
                                <li><a href="blog-2.html">Pantau Pengaduan</a></li>
                                <li><a href="blog-2.html">Grafik Pengaduan</a></li>
                            </ul>
                        </li>
                    <li><a href="<?php echo base_url();?>index.php/login">LOGIN</a></li>
                
                </ul>
            </div>
        </div><!-- /.container -->
    </nav>
       
    <section id="contribution" class="section bg-image-2 contribution">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="text-uppercase mt0 font-650">TATA CARA PENGADUAN</h1>
                    <h2 >Berikut ini adalah penjelasan tentang tata cara  Mengirim Pengaduan</h2>
                </div>
            </div>
        </div>
    </section>

    <section id="faq" class="section faq">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Whistleblower</h2>
                </div>
            </div>
             <p>Pelapor pelanggaran (whistleblower) adalah istilah bagi karyawan, mantan karyawan atau pekerja, anggota dari suatu institusi atau organisasi yang melaporkan suatu tindakan yang dianggap melanggar ketentuan kepada pihak yang berwenang. Secara umum segala tindakan yang melanggar ketentuan berarti melanggar hukum, aturan dan persyaratan yang menjadi ancaman pihak publik atau kepentingan publik. Termasuk di dalamnya korupsi, pelanggaran atas keselamatan kerja, dan masih banyak lagi. </p>

             <div class="row">
             <div class="col-md-12">
                    <h2>Tata cara Pengaduan</h2>
                </div>
            </div>
                    Berikut ini adalah langkah-langkah yang dilakukan untuk membuat pengaduan melalui aplikasi WBS (Whistleblowing System) SPI UINSA, yaitu:

                    <ol>
                    <li>
                     Sebelum melaporkan pengaduan Anda di Whistleblowing System SPI UINSA, terlebih dahulu periksa kelengkapan pengaduan Anda apakah telah sesuai dengan kriteria pengaduan yang telah ditetapkan yaitu mengandung unsur <strong>4W+1H</strong> (What, Who, Where, When dan How). Lebih jelas tentang kriteria pengaduan dapat dilihat di tautan berikut ini : <a href="index.php?halaman=tentang" target="_blank">Kriteria Pengaduan</a>.</li> <br>
                    <li>
                    Jika pengaduan tersebut telah memenuhi kriteria yang telah ditentukan, tahap berikutnya adalah mengisi formulir pengaduan dengan menekan tombol <a href="index.php?halaman=pengaduan" target="_blank">KIRIM-PENGADUAN</a> yang terdapat pada menu navigasi sebelah kanan atas halaman. Silahkan mengisi semua data yang diminta secara lengkap dan benar dan lanjutkan dengan menekan tombol &quot;Kirim-Pengaduan&quot; yang muncul setelah mencentang konfirmasi validasi data pengaduan.</li><br>
                    <li>
                    Setelah mengirim pengaduan, secara otomatis anda telah membuat akun di aplikasi WBS (Whistleblowing System) SPI UINSA dan Anda akan mendapat kode unik terkait laporan yang dikirim. Akun dan kode unik tersebut dapat digunakan untuk masuk ke halaman khusus pelapor sehingga dapat memantau tahapan proses pengaduan. (<strong>Jaga kerahasiaan kode akses akun dan kode unik penguan Anda, agar tidak disalahgunakan oleh orang yang tidak bertanggung jawab</strong>)</li><br>
                    <li>
                    Anda dapat memantau pengaduan yang pernah dikirim, membuat pengaduan baru dan juga dapat melakukan <strong>komunikasi secara pribadi dengan administrator WBS</strong> melalui halaman khusus pelapor. &quot;<strong>Komunikasi secara pribadi dengan administrator WBS</strong>&quot; melalui halaman khusus pelapor menjamin data pribadi Anda tidak akan diketahui dan dijaga kerahasiaannya. Kami tidak meminta data pribadi yang berhubungan dengan Anda secara langsung kecuali jika tindak lanjut dari pengaduan tersebut membutuhkan data pribadi Anda.</li>
                    </ol><br>


                <div class="row">
                <div class="col-md-12">
                </div>
            </div>
             <strong>Sebagai bentuk terimakasih kami terhadap laporan yang Anda kirim, kami berkomitmen untuk merespon laporan Anda selambat-lambatnya 7 (tujuh) hari kerja sejak laporan Anda dikirim</strong>

            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>




    <footer class="site-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="site-info">WHISTLE BLOWING SYSTEM INTERNAL UINSA <br>
                        SATUAN PENGAWAS INTERNAL 
                        <br>JL. A. Yani 117, Surabaya Jawa Timur, Indonesia, 60237
                        <br>Telp. +62 31 8410298 ext.141 <br>Fax. +62 31 8413300
                        <br> </p><img src="<?php echo base_url(); ?>assets/images/logo1.png" alt="Logo">





                    <ul class="social-block">
                        <li><a href=""><i class="ion-social-twitter"></i></a></li>
                        <li><a href=""><i class="ion-social-facebook"></i></a></li>
                        <li><a href=""><i class="ion-social-linkedin-outline"></i></a></li>
                        <li><a href=""><i class="ion-social-googleplus"></i></a></li>
                    </ul>
                        <div class="footer2">Hak cipta &copy; 2018 SATUAN PENGAWAS INTERNAL UINSA <br>- All Rights Reserved -</div>

</div>
                </div>
            </div>
        </div>
    </footer>

    <!-- script -->
    <script src="<?php echo base_url(); ?>assets/jquery/dist/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/smooth-scroll/dist/js/smooth-scroll.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/main.js"></script>
</body>
</html>