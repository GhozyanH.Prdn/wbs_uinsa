<?php
class Blog_model_undangan extends CI_Model{

	function get_all_blog(){
		$result=$this->db->get('laporan');
		return $result;
	}

	function search_blog($title){
		$this->db->like('kode_lapor', $title , 'both');
		$this->db->order_by('kode_lapor', 'ASC');
		$this->db->limit(10);
		$this->db->where('status','accepted');
		return $this->db->get('laporan')->result();
	}

}