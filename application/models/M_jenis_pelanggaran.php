<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_jenis_pelanggaran extends CI_Model
{
	
	var $table = 'pelanggaran'; // database for model books

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	public function get_all_books()
	{
		$this->db->from('pelanggaran');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_by_id($id_pelanggaran)
	{
		$this->db->from($this->table);
		$this->db->where('id_pelanggaran',$id_pelanggaran);
		$query = $this->db->get();

		return $query->row();
	}

	public function book_add($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function book_update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id_pelanggaran)
	{
		$this->db->where('id_pelanggaran', $id_pelanggaran);
		$this->db->delete($this->table);
	}
}