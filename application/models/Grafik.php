<?php
// Penduduk.php
class Grafik extends CI_Model {
	public function __construct()
	{
		$this->load->database();
	}
 
	public function graph()
	{
		$data = $this->db->query("SELECT * FROM laporan");
		return $data->result();
	}



	public function hitungJumlahInventori()
{
   $query = $this->db->get('tbpegawai');
    if($query->num_rows()>0)
    {
      return $query->num_rows();
    }
    else
    {
      return 0;
    }
}


	public function hitungJumlahData()
{   
    $query = $this->db->get('non_civitas');
    if($query->num_rows()>0)
    {
      return $query->num_rows();
    }
    else
    {
      return 0;
    }
}

	
	public function hitungJumlahSelesai()
{   
    
    $this->db->from('laporan');
    $this->db->where('status_verivikasi','SELESAI');
	$query = $this->db->get();

	// $query = $this->db->get('laporan');
 //    $query = $this->db->WHERE('status_verifikasi','SELESAI');
    if($query->num_rows()>0)
    {
      return $query->num_rows();
    }
    else
    {
      return 0;
    }
}


	public function hitungJumlahLolos()
{   
    
    $this->db->from('laporan');
    $this->db->where('status','Accepted');
	$query = $this->db->get();

	// $query = $this->db->get('laporan');
 //    $query = $this->db->WHERE('status_verifikasi','SELESAI');
    if($query->num_rows()>0)
    {
      return $query->num_rows();
    }
    else
    {
      return 0;
    }
}
 
}