<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Books_model_undangan extends CI_Model
{
	
	var $table = 'undangan'; // database for model books

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	public function get_all_books()
	{
		$this->db->from('undangan');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_by_id($id_undangan)
	{
		$this->db->from($this->table);
		$this->db->where('id_undangan',$id_undangan);
		$query = $this->db->get();

		return $query->row();
	}

	public function book_add($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function book_update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id_undangan)
	{
		$this->db->where('id_undangan', $id_undangan);
		$this->db->delete($this->table);
	}
}