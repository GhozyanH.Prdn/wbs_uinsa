<?php
class Blog_model_unit extends CI_Model{

	function get_all_blog(){
		$result=$this->db->get('m_unit');
		return $result;
	}

	function search_blog($title){
		$this->db->like('nama_unit', $title , 'both');
		$this->db->order_by('nama_unit', 'ASC');
		$this->db->limit(10);
		return $this->db->get('m_unit')->result();
	}

}