<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Books_model_unit2 extends CI_Model
{
	
	var $table = 'laporan_unit'; // database for model books

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	public function get_all_books()
	{
		$this->db->from('laporan_unit');
		$this->db->where('id',$this->session->userdata('ses_id'));
		$query = $this->db->get();
		return $query->result();
	}

	public function get_by_id($id_laporan_unit)
	{
		$this->db->from($this->table);
		$this->db->where('id_laporan_unit',$id_laporan_unit);
		$query = $this->db->get();

		return $query->row();
	}

	public function book_add($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function book_update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id_laporan_unit)
	{
		$this->db->where('id_laporan_unit', $id_laporan_unit);
		$this->db->delete($this->table);
	}
}