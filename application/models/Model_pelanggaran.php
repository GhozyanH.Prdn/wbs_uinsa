<?php
class model_pelanggaran extends CI_Model {
 public function get(){
  return $this->db->get('pelanggaran');

    }
     function kode_pelanggaran() {
        $q = $this->db->query("SELECT MAX(RIGHT(id_pelanggaran,4)) AS kd_max FROM pelanggaran WHERE DATE(tgl)=CURDATE()");
        $kd = "";
        if($q->num_rows()>0) {
            foreach($q->result() as $k) {
                $tmp = ((int)$k->kd_max)+1;
                $kd = sprintf("%04s", $tmp);
            }
        }else{
            $kd = "0001";
        }
        date_default_timezone_set('Asia/Jakarta');
        return date('dmy').$kd;
    }
}
?>