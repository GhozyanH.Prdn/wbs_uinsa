<?php
defined('BASEPATH') OR exit('No Direct Script allowed');

class data_perorangan extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('books3_model');
		if($this->session->userdata('masuk') != TRUE){
			$url=base_url();
			redirect($url);
		}
		  }

	public function index()
	{	if($this->session->userdata('akses')=='2'){
		$data['books'] = $this->books3_model->get_all_books();
		$this->load->view('dosen/data_perorangan',$data);
    }else{
    	$this->load->view('warning');
    }
		
	}
	
	public function book_add()
	{
		$data = array(
			'id' 			=> $this->input->post('id'),
			'kode_lapor'		=> $this->input->post('kode_lapor'),
			'tgl_lapor' 		=> $this->input->post('tgl_lapor'),
			'nama' 				=> $this->input->post('nama'),
			'nip' 				=> $this->input->post('nip'),
			'pelanggaran' 		=> $this->input->post('pelanggaran'),
			'tempat_kejadian' 	=> $this->input->post('tempat_kejadian'),
			'tanggal' 			=> $this->input->post('tanggal'),
			'uraian' 			=> $this->input->post('uraian'),
			'bukti' 			=> $this->input->post('bukti'),
			'status' 			=> $this->input->post('status'),
			'status_verivikasi' => $this->input->post('status_verivikasi'),
			'tgl_undangan' 		=> $this->input->post('tgl_undangan'),
			);
		$insert = $this->books3_model->book_add($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_edit($id_laporan)
	{
		$data = $this->books3_model->get_by_id($id_laporan);
		echo json_encode($data);
	}

	public function book_update()
	{
		$data = array(
			'id' 			=> $this->input->post('id'),
			'kode_lapor'		=> $this->input->post('kode_lapor'),
			'tgl_lapor' 		=> $this->input->post('tgl_lapor'),
			'nama' 				=> $this->input->post('nama'),
			'nip' 				=> $this->input->post('nip'),
			'pelanggaran' 		=> $this->input->post('pelanggaran'),
			'tempat_kejadian' 	=> $this->input->post('tempat_kejadian'),
			'tanggal' 			=> $this->input->post('tanggal'),
			'uraian' 			=> $this->input->post('uraian'),
			'bukti' 			=> $this->input->post('bukti'),
			'status' 			=> $this->input->post('status'),
			'status_verivikasi' => $this->input->post('status_verivikasi'),
			'tgl_undangan' 		=> $this->input->post('tgl_undangan'),
			);
		$this->books3_model->book_update(array('id_laporan' => $this->input->post('id_laporan')), $data);
		echo json_encode(array("status" => TRUE));
	}
	
	public function book_delete($id_laporan)
	{
		$this->books3_model->delete_by_id($id_laporan);
		echo json_encode(array("status" => TRUE));
    }
}