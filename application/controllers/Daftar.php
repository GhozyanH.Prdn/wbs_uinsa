<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Daftar extends CI_Controller {
	function __construct() {
		parent::__construct();		
		$this->load->helper(array('url'));

	}
	public function index()
	{
		$this->load->view('daftar');
	}

	public function aksi_upload(){
		$config['upload_path']          = './assets/images/non_civitas/';
		$config['allowed_types']        = 'gif|jpg|png';
		$config['max_size']             = 100000;
		$config['max_width']            = 50000;
		$config['max_height']           = 50000;
 
		$this->load->library('upload', $config);
 
		if ( ! $this->upload->do_upload())
		{
			$this->load->view('V_error');
			
		}
		else{
			$img = $this->upload->data();
			$foto = $img['file_name'];
			$nama = $this->input->post('nama');
			$nama_akhir = $this->input->post('nama_akhir');
			$email = $this->input->post('email');
			$alamat = $this->input->post('alamat');
			$pos = $this->input->post('pos');
			$telp = $this->input->post('telp');
			$jk = $this->input->post('jk');
			$pass = $this->input->post('pass');
			$level = $this->input->post('level');

			$data = array(
				'nama' => $nama,
				'nama_akhir' => $nama_akhir,
				'email' => $email,
				'alamat' => $alamat,
				'pos' => $pos,
				'telp' => $telp,
				'jk' => $jk,
				'foto' => $foto,
				'pass' => md5($pass),
				'level' => $level,
			);
			$this->db->insert('non_civitas',$data);
			$this->load->view('v_login');
			redirect('login');
		}
	}
}
