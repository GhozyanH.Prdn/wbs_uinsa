<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class buat_undangan extends CI_Controller {
	 function __construct(){
    parent::__construct();
    $this->load->helper(array('url'));  
    $this->load->model ('M_undangan');
    $this->load->model('Blog_model_undangan');
	error_reporting(0);
    //validasi jika user belum login
    if($this->session->userdata('masuk') != TRUE){
			$url=base_url();
			redirect($url);
		}
		  }

	public function index()
	{
		if($this->session->userdata('akses')=='1'){
	$data['kode_undangan'] = $this->M_undangan->kode_undangan();
    $this->load->view('admin/buat_undangan',$data);
    }else{
     $this->load->view('warning');
}
}
	function get_autocomplete(){
		if (isset($_GET['term'])) {
		  	$result = $this->Blog_model_undangan->search_blog($_GET['term']);
		   	if (count($result) > 0) {
		    foreach ($result as $row)
		     	$arr_result[] = array(
					'label'			=> $row->kode_lapor,
					'description'	=> $row->nama,
				);
		     	echo json_encode($arr_result);
		   	}
		}
}

    public function upload(){
			$kode_undangan = $this->input->post('kode_undangan');
			$kode_lapor = $this->input->post('kode_lapor');
			$nama = $this->input->post('nama');
			$tgl_dibuat = $this->input->post('tgl_dibuat');
			$tgl_undangan = $this->input->post('tgl_undangan');
			$pukul = $this->input->post('pukul');
			
			$data = array(
				'kode_undangan' => $kode_undangan,
				'kode_lapor' => $kode_lapor,
				'nama' => $nama,
				'tgl_dibuat' => $tgl_dibuat,
				'tgl_undangan' => $tgl_undangan,
				'pukul' => $pukul,	
			);
			$this->db->insert('undangan',$data);
			$this->load->view('admin/undangan');
			redirect('admin/undangan');
		}
}
