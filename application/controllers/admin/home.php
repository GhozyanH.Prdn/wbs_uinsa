<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	 function __construct(){
    parent::__construct();
    $this->load->model('Grafik');
    //validasi jika user belum login
    if($this->session->userdata('masuk') != TRUE){
			$url=base_url();
			redirect($url);
		}
		  }

	public function index()
	{
		if($this->session->userdata('akses')=='1'){
		$data['graph'] = $this->Grafik->graph();
		$data['total_asset'] = $this->Grafik->hitungJumlahInventori();
		$data['total_data'] = $this->Grafik->hitungJumlahData();
		$data['total_selesai'] = $this->Grafik->hitungJumlahSelesai();
		$data['total_lolos'] = $this->Grafik->hitungJumlahLolos();
      $this->load->view('admin/home',$data);
    }else{
    	$this->load->view('warning');
    }
}	
}
