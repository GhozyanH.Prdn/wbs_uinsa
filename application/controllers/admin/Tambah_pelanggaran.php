<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tambah_pelanggaran extends CI_Controller {
	 function __construct(){
    parent::__construct();
    $this->load->helper(array('url'));  
    $this->load->model ('Model_pelanggaran');
    //validasi jika user belum login
    if($this->session->userdata('masuk') != TRUE){
			$url=base_url();
			redirect($url);
		}
		  }

	public function index()
	{
		if($this->session->userdata('akses')=='1'){
	$data['kode_pelanggaran'] = $this->Model_pelanggaran->kode_pelanggaran();
    $this->load->view('admin/tambah_pelanggaran',$data);
    }else{
     $this->load->view('warning');
}
}

    public function upload(){
			$kode_pelanggaran = $this->input->post('kode_pelanggaran');
			$nama_pelanggaran = $this->input->post('nama_pelanggaran');
			$tgl = $this->input->post('tgl');
			
			$data = array(
				'kode_pelanggaran' => $kode_pelanggaran,
				'nama_pelanggaran' => $nama_pelanggaran,
				'tgl' => $tgl,
				
			);
			$this->db->insert('pelanggaran',$data);
			$this->load->view('admin/jenis_pelanggaran');
			redirect('admin/jenis_pelanggaran');
		}
}
