<?php
defined('BASEPATH') OR exit('No Direct Script allowed');

class laporan_masuk extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('pdf');
		$this->load->helper('url');
		$this->load->model('Books5_model');
		if($this->session->userdata('masuk') != TRUE){
			$url=base_url();
			redirect($url);
		}
		  }

	public function index()
	{	if($this->session->userdata('akses')=='1'){
		$data['books'] = $this->Books5_model->get_all_books();
		$this->load->view('admin/laporan_masuk',$data);
    }else{
    	$this->load->view('warning');
    }
		
	}
	
	public function book_add()
	{
		$data = array(
			'id' 			=> $this->input->post('id'),
			'kode_lapor'		=> $this->input->post('kode_lapor'),
			'tgl_lapor' 		=> $this->input->post('tgl_lapor'),
			'nama' 				=> $this->input->post('nama'),
			'nip' 				=> $this->input->post('nip'),
			'pelanggaran' 		=> $this->input->post('pelanggaran'),
			'tempat_kejadian' 	=> $this->input->post('tempat_kejadian'),
			'tanggal' 			=> $this->input->post('tanggal'),
			'uraian' 			=> $this->input->post('uraian'),
			'bukti' 			=> $this->input->post('bukti'),
			'status' 			=> $this->input->post('status'),
			'status_verivikasi' => $this->input->post('status_verivikasi'),
			'tgl_undangan' 		=> $this->input->post('tgl_undangan'),
			);
		$insert = $this->Books5_model->book_add($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_edit($id_laporan)
	{
		$data = $this->Books5_model->get_by_id($id_laporan);
		echo json_encode($data);
	}

	public function book_update()
	{
		$data = array(
			'id' 			=> $this->input->post('id'),
			'kode_lapor'		=> $this->input->post('kode_lapor'),
			'tgl_lapor' 		=> $this->input->post('tgl_lapor'),
			'nama' 	=> $this->input->post('nama'),
			'nip' 			=> $this->input->post('nip'),
			'pelanggaran' 		=> $this->input->post('pelanggaran'),
			'tempat_kejadian' 	=> $this->input->post('tempat_kejadian'),
			'tanggal' 			=> $this->input->post('tanggal'),
			'uraian' 			=> $this->input->post('uraian'),
			'bukti' 			=> $this->input->post('bukti'),
			'status' 			=> $this->input->post('status'),
			'status_verivikasi' => $this->input->post('status_verivikasi'),
			'tgl_undangan' 		=> $this->input->post('tgl_undangan'),
			);
		$this->Books5_model->book_update(array('id_laporan' => $this->input->post('id_laporan')), $data);
		echo json_encode(array("status" => TRUE));
	}
	
	public function book_delete($id_laporan)
	{
		$this->Books5_model->delete_by_id($id_laporan);
		echo json_encode(array("status" => TRUE));
	}


	
    
    function cetak(){
        $pdf = new FPDF('P','mm','A4');
        // membuat halaman baru
		$pdf->AddPage();
		$pdf->Image('assets/images/logo2.png',10,4,27,27);
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial','B',16);
		// mencetak string 
		$pdf->Cell(200,0,'KEMENTERIAN AGAMA',0,1,'C');
		$pdf->Cell(210,10,'UNIVERSITAS ISLAM NEGERI SUNAN AMPEL SURABAYA',0,1,'C');
		$pdf->SetFont('Arial','B',14);
		$pdf->Cell(200,7,'SATUAN PENGAWAS INTERNAL',0,1,'C');
		$pdf->SetFont('Arial','B',8);
		$pdf->Cell(200,2,'Jl. Jend. A. Yani 117 Telp./Fax. 031-8420118 60237 Website; http://spi.uinsby.ac.id Email; spi@uinsby.ac.id',0,1,'C');

		$pdf->Ln(10);
		$pdf->SetLineWidth(0);
    	$pdf->Line(10,32,200,32);
    	$pdf->SetLineWidth(1);
    	$pdf->Line(10,33,200,33);
    	$pdf->SetLineWidth(0);
    	$pdf->Line(10,34,200,34);
		$pdf->SetLineWidth(0);

		$pdf->Ln();
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(20,1,'Nomor');
		$pdf->Cell(0.1);
		$pdf->SetFont('Arial','B',11);
		$pdf->Cell(10,1,':');

		$pdf->Ln();
		$pdf->Cell(120);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(20,1,'Tanggal');
		
		$pdf->SetFont('Arial','B',11);
		$pdf->Cell(10,1,':');

		$pdf->Ln(5);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(20,7,'Lamp');
		$pdf->Cell(0.1);
		$pdf->SetFont('Arial','B',11);
		$pdf->Cell(10,7,':');
		
		$pdf->Ln(5);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(20,13,'Perihal');
		$pdf->Cell(0.1);
		$pdf->SetFont('Arial','B',11);
		$pdf->Cell(5,13,':');
		$pdf->Cell(1,13,'Undangan Pemeriksaan Oleh SPI');

		$pdf->Ln(30);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','i',12);
		$pdf->Cell(30,2,'Kepada YTH,');

		$pdf->Ln(5);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','B',12);
		$pdf->Cell(10,4,'$nama');

		$pdf->Ln(5);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','',12);
		$pdf->Cell(10,6,'Di Tempat');

		$pdf->Ln(15);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','',11);
		$pdf->Cell(10,2,'Dengan Hormat,');

		$pdf->Ln(5);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,'Sehubungan dengan SK Rektor No 13 Tahun 2017, yang mengatur tentang WhistleBlowing.');

		$pdf->Ln(5);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,'Maka Saya yang bertanda tangan dibawah ini, mengundang Saudara Untuk dilaksaakan Pemeriksaan');

		$pdf->Ln(5);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,'Untuk hadir pada :');

		$pdf->Ln(10);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,'Hari/ tanggal');
		$pdf->Cell(20);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,':');

		$pdf->Ln(7);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,'Pukul');
		$pdf->Cell(20);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,':');

		$pdf->Ln(7);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,'Tempat');
		$pdf->Cell(20);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,': Gedung Twin Towers A Lt. 5');
		$pdf->Ln(5);
		$pdf->Cell(42);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,'Universitas Islam Negeri Sunan Ampel Surabaya');

		$pdf->Ln(7);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,'Acara');
		$pdf->Cell(20);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,': Pemeriksaan Berita Acara Pelaporan Whistle Blowing System');
		
		$pdf->Ln(15);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,'Demikian surat undangan ini kami sampaikan, atas kerjasamanya kami ucapkan terimakasih');

		$pdf->Ln(25);
		$pdf->Cell(135);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,'a.n Ketua SPI');

		$pdf->Ln(20);
		$pdf->Cell(135);
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(10,2,'Drs. Sutikno, M.Pd.I');

		$pdf->Ln(5);
		$pdf->Cell(135);
		$pdf->SetFont('Arial','BU',10);
		$pdf->Cell(10,2,'196808061994031003');

		$pdf->Ln(10);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','bu',10);
		$pdf->Cell(10,2,'Tembusan:');

		$pdf->Ln(5);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,'1. Rektor');

		$pdf->Ln(5);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,'2. Wakil Rektor');

		$pdf->Ln(5);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,'3. Ketua SPI');

		$pdf->Ln(5);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,'4. Sekertaris SPI');

		$pdf->Ln(5);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,'5. Arsip');


		
		


	
		
        
		$pdf->Output();

		
    }
}