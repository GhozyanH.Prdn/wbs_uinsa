<?php
defined('BASEPATH') OR exit('No Direct Script allowed');

class laporan_accepted extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('pdf');
		$this->load->helper('url');
		$this->load->model('books_model');
		if($this->session->userdata('masuk') != TRUE){
			$url=base_url();
			redirect($url);
		}
		  }

	public function index()
	{	if($this->session->userdata('akses')=='1'){
		$data['books'] = $this->books_model->get_all_books();
		$this->load->view('admin/laporan_accepted',$data);
    }else{
    	$this->load->view('warning');
    }
		
	}
	
	public function book_add()
	{
		$data = array(
			'id' 			=> $this->input->post('id'),
			'kode_lapor'		=> $this->input->post('kode_lapor'),
			'tgl_lapor' 		=> $this->input->post('tgl_lapor'),
			'nama' 				=> $this->input->post('nama'),
			'nip' 				=> $this->input->post('nip'),
			'pelanggaran' 		=> $this->input->post('pelanggaran'),
			'tempat_kejadian' 	=> $this->input->post('tempat_kejadian'),
			'tanggal' 			=> $this->input->post('tanggal'),
			'uraian' 			=> $this->input->post('uraian'),
			'bukti' 			=> $this->input->post('bukti'),
			'status' 			=> $this->input->post('status'),
			'status_verivikasi' => $this->input->post('status_verivikasi'),
			'tgl_undangan' 		=> $this->input->post('tgl_undangan'),
			);
		$insert = $this->books_model->book_add($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_edit($id_laporan)
	{
		$data = $this->books_model->get_by_id($id_laporan);
		echo json_encode($data);
	}

	public function book_update()
	{
		$data = array(
			'id' 			=> $this->input->post('id'),
			'kode_lapor'		=> $this->input->post('kode_lapor'),
			'tgl_lapor' 		=> $this->input->post('tgl_lapor'),
			'nama' 				=> $this->input->post('nama'),
			'nip' 				=> $this->input->post('nip'),
			'pelanggaran' 		=> $this->input->post('pelanggaran'),
			'tempat_kejadian' 	=> $this->input->post('tempat_kejadian'),
			'tanggal' 			=> $this->input->post('tanggal'),
			'uraian' 			=> $this->input->post('uraian'),
			'bukti' 			=> $this->input->post('bukti'),
			'status' 			=> $this->input->post('status'),
			'status_verivikasi' => $this->input->post('status_verivikasi'),
			'tgl_undangan' 		=> $this->input->post('tgl_undangan'),
			);
		$this->books_model->book_update(array('id_laporan' => $this->input->post('id_laporan')), $data);
		echo json_encode(array("status" => TRUE));
	}
	
	public function book_delete($id_laporan)
	{
		$this->books_model->delete_by_id($id_laporan);
		echo json_encode(array("status" => TRUE));
	}


    
    function cetak(){
        $pdf = new FPDF('l','mm','A5');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial','B',16);
        // mencetak string 
        $pdf->Cell(190,7,'Laporan masuk',0,1,'C');
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(190,7,'Whistle Blowing UINSA',0,1,'C');
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10,7,'',0,1);
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(20,6,'NIM',1,0);
        $pdf->Cell(85,6,'NAMA MAHASISWA',1,0);
        $pdf->Cell(27,6,'NO HP',1,0);
        $pdf->Cell(25,6,'TANGGAL LHR',1,1);
        $pdf->SetFont('Arial','',10);
        $mahasiswa = $this->db->get('laporan')->result();
        foreach ($mahasiswa as $row){
            $pdf->Cell(20,6,$row->kode_lapor,1,0);
            $pdf->Cell(85,6,$row->tgl_lapor,1,0);
            $pdf->Cell(27,6,$row->nama_terlapor,1,0);
            $pdf->Cell(25,6,$row->pelanggaran,1,1); 
        }
        $pdf->Output();
    }
}