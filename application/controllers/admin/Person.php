<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Person extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('pdf');
		$this->load->helper('url');
		$this->load->model('person_model','person');
		if($this->session->userdata('masuk') != TRUE){
			$url=base_url();
			redirect($url);
		}
	}

	public function index()
	{	if($this->session->userdata('akses')=='1'){
		$this->load->view('admin/person_view');
    }else{
    	$this->load->view('warning');
    }
		
	}

	public function ajax_list()
	{
		$this->load->helper('url');

		$list = $this->person->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $person) {
			$no++;
			$row = array();
			$row[] = $person->kode_lapor;
			$row[] = $person->nama;
			$row[] = $person->kode_undangan;
			$row[] = $person->tgl_dibuat;
			$row[] = $person->tgl_undangan;
			$row[] = $person->pukul;
			if($person->photo)
				$row[] = '<a href="'.base_url('assets/images/scan_undangan/'.$person->photo).'" target="_blank"><img src="'.base_url('assets/images/scan_undangan/'.$person->photo).'" class="img-responsive" /></a>';
			else
				$row[] = '(No File selected)';

			//add html for action
			
			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_person('."'".$person->id_undangan."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
					<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="print" onclick="print_person('."'".$person->id_undangan."'".')"><i class="glyphicon glyphicon-print"></i> print</a>
				  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('."'".$person->id_undangan."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->person->count_all(),
						"recordsFiltered" => $this->person->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($id_undangan)
	{
		$data = $this->person->get_by_id($id_undangan);
		$data->tgl_undangan = ($data->tgl_undangan == '0000-00-00') ? '' : $data->tgl_undangan; // if 0000-00-00 set tu empty for datepicker compatibility
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$this->_validate();
		
		$data = array(
				'kode_lapor' => $this->input->post('kode_lapor'),
				'nama' => $this->input->post('nama'),
				'kode_undangan' => $this->input->post('kode_undangan'),
				'tgl_dibuat' => $this->input->post('tgl_dibuat'),
				'tgl_undangan' => $this->input->post('tgl_undangan'),
				'pukul' => $this->input->post('pukul'),
				
			);

		if(!empty($_FILES['photo']['name']))
		{
			$upload = $this->_do_upload();
			$data['photo'] = $upload;
		}

		$insert = $this->person->save($data);

		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$this->_validate();
		$data = array(
				'kode_lapor' => $this->input->post('kode_lapor'),
				'nama' => $this->input->post('nama'),
				'kode_undangan' => $this->input->post('kode_undangan'),
				'tgl_dibuat' => $this->input->post('tgl_dibuat'),
				'tgl_undangan' => $this->input->post('tgl_undangan'),
				'pukul' => $this->input->post('pukul'),
			);

		if($this->input->post('remove_photo')) // if remove photo checked
		{
			if(file_exists('assets/images/scan_undangan/'.$this->input->post('remove_photo')) && $this->input->post('remove_photo'))
				unlink('assets/images/scan_undangan/'.$this->input->post('remove_photo'));
			$data['photo'] = '';
		}

		if(!empty($_FILES['photo']['name']))
		{
			$upload = $this->_do_upload();
			
			//delete file
			$person = $this->person->get_by_id($this->input->post('id_undangan'));
			if(file_exists('assets/images/scan_undangan/'.$person->photo) && $person->photo)
				unlink('assets/images/scan_undangan/'.$person->photo);

			$data['photo'] = $upload;
		}

		$this->person->update(array('id_undangan' => $this->input->post('id_undangan')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($id_undangan)
	{
		//delete file
		$person = $this->person->get_by_id($id_undangan);
		if(file_exists('assets/images/scan_undangan/'.$person->photo) && $person->photo)
			unlink('assets/images/scan_undangan/'.$person->photo);
		
		$this->person->delete_by_id($id_undangan);
		echo json_encode(array("status" => TRUE));
	}
	public function cetak()
	{
		//delete file
		$id_undangan = $this->uri->segment(4);
		$this->db->from('undangan');
		$this->db->where('id_undangan',$id_undangan);
		$query = $this->db->get();
		$undangan = $query->result();
		foreach ($undangan as $row){
		
		$pdf = new FPDF('P','mm','A4');
		$pdf->AddPage();
		$pdf->Image('assets/images/logo2.png',10,4,27,27);
        $pdf->SetFont('Arial','B',16);
		$pdf->Cell(200,0,'KEMENTERIAN AGAMA',0,1,'C');
		$pdf->Cell(210,10,'UNIVERSITAS ISLAM NEGERI SUNAN AMPEL SURABAYA',0,1,'C');
		$pdf->SetFont('Arial','B',14);
		$pdf->Cell(200,7,'SATUAN PENGAWAS INTERNAL',0,1,'C');
		$pdf->SetFont('Arial','B',8);
		$pdf->Cell(200,2,'Jl. Jend. A. Yani 117 Telp./Fax. 031-8420118 60237 Website; http://spi.uinsby.ac.id Email; spi@uinsby.ac.id',0,1,'C');

		$pdf->Ln(10);
		$pdf->SetLineWidth(0);
    	$pdf->Line(10,32,200,32);
    	$pdf->SetLineWidth(1);
    	$pdf->Line(10,33,200,33);
    	$pdf->SetLineWidth(0);
    	$pdf->Line(10,34,200,34);
		$pdf->SetLineWidth(0);

		$pdf->Ln();
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(20,1,'Nomor');
		$pdf->Cell(0.1);
		$pdf->SetFont('Arial','B',11);
		$pdf->Cell(5,1,':');
		$pdf->Cell(1,1,$row->kode_lapor);

		$pdf->Ln();
		$pdf->Cell(120);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(20,1,'Tanggal');
		
		$pdf->SetFont('Arial','B',11);
		$pdf->Cell(3,1,':');
		$pdf->Cell(20,1,$row->tgl_dibuat);

		$pdf->Ln(5);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(20,7,'Lamp');
		$pdf->Cell(0.1);
		$pdf->SetFont('Arial','B',11);
		$pdf->Cell(5,7,':');
		$pdf->Cell(1,7,' - ');
		
		$pdf->Ln(5);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(20,13,'Perihal');
		$pdf->Cell(0.1);
		$pdf->SetFont('Arial','B',11);
		$pdf->Cell(5,13,':');
		$pdf->Cell(1,13,'Undangan Pemeriksaan Oleh SPI');

		$pdf->Ln(30);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','i',12);
		$pdf->Cell(30,2,'Kepada YTH,');

		$pdf->Ln(5);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','B',12);
		$pdf->Cell(20,6,$row->nama);
		

		$pdf->Ln(5);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','',12);
		$pdf->Cell(10,6,'Di Tempat');

		$pdf->Ln(15);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','',11);
		$pdf->Cell(10,2,'Dengan Hormat,');

		$pdf->Ln(5);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,'Sehubungan dengan SK Rektor No 13 Tahun 2017, yang mengatur tentang WhistleBlowing.');

		$pdf->Ln(5);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,'Maka Saya yang bertanda tangan dibawah ini, mengundang Saudara Untuk dilaksaakan Pemeriksaan');

		$pdf->Ln(5);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,'Untuk hadir pada :');

		$pdf->Ln(10);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,'Hari/ tanggal');
		$pdf->Cell(20);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(2,1,':');
		$pdf->Cell(2,1,$row->tgl_undangan);

		$pdf->Ln(7);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,'Pukul');
		$pdf->Cell(20);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(2,1,':');
		$pdf->Cell(2,1,$row->pukul);

		$pdf->Ln(7);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,'Tempat');
		$pdf->Cell(20);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,': Gedung Twin Towers A Lt. 5');
		$pdf->Ln(5);
		$pdf->Cell(42);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,'Universitas Islam Negeri Sunan Ampel Surabaya');

		$pdf->Ln(7);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,'Acara');
		$pdf->Cell(20);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,': Pemeriksaan Berita Acara Peundangan Whistle Blowing System');
		
		$pdf->Ln(15);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,'Demikian surat undangan ini kami sampaikan, atas kerjasamanya kami ucapkan terimakasih');

		$pdf->Ln(25);
		$pdf->Cell(135);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,'a.n Ketua SPI');

		$pdf->Ln(20);
		$pdf->Cell(135);
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(10,2,'Drs. Sutikno, M.Pd.I');

		$pdf->Ln(5);
		$pdf->Cell(135);
		$pdf->SetFont('Arial','BU',10);
		$pdf->Cell(10,2,'196808061994031003');

		$pdf->Ln(10);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','bu',10);
		$pdf->Cell(10,2,'Tembusan:');

		$pdf->Ln(5);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,'1. Rektor');

		$pdf->Ln(5);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,'2. Wakil Rektor');

		$pdf->Ln(5);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,'3. Ketua SPI');

		$pdf->Ln(5);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,'4. Sekertaris SPI');

		$pdf->Ln(5);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(10,2,'5. Arsip');


		$pdf->Output();	

		echo json_encode(array("status" => TRUE));

		}
	}

	private function _do_upload()
	{
		$config['upload_path']          = 'assets/images/scan_undangan/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['max_size']             = 1000000000; //set max size allowed in Kilobyte
        $config['max_width']            = 1000000; // set max width image allowed
        $config['max_height']           = 1000000; // set max height allowed
        $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name

        $this->load->library('upload', $config);

        if(!$this->upload->do_upload('photo')) //upload and validate
        {
            $data['inputerror'][] = 'photo';
			$data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
			$data['status'] = FALSE;
			echo json_encode($data);
			exit();
		}
		return $this->upload->data('file_name');
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('kode_lapor') == '')
		{
			$data['inputerror'][] = 'kode_lapor';
			$data['error_string'][] = 'First name is required';
			$data['status'] = FALSE;
		}

		if($this->input->post('nama') == '')
		{
			$data['inputerror'][] = 'nama';
			$data['error_string'][] = 'Last name is required';
			$data['status'] = FALSE;
		}

		if($this->input->post('kode_undangan') == '')
		{
			$data['inputerror'][] = 'kode_undangan';
			$data['error_string'][] = 'Date of Birth is required';
			$data['status'] = FALSE;
		}

		if($this->input->post('tgl_dibuat') == '')
		{
			$data['inputerror'][] = 'tgl_dibuat';
			$data['error_string'][] = 'Please select gender';
			$data['status'] = FALSE;
		}

		if($this->input->post('tgl_undangan') == '')
		{
			$data['inputerror'][] = 'tgl_undangan';
			$data['error_string'][] = 'Addess is required';
			$data['status'] = FALSE;
		}
		if($this->input->post('pukul') == '')
		{
			$data['inputerror'][] = 'pukul';
			$data['error_string'][] = 'Addess is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
    }
    }
