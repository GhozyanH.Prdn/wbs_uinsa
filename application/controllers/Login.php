<?php
class Login extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->model('login_model');
	}

	function index(){
		$this->load->view('v_login');
	}

	function auth(){
        $username=htmlspecialchars($this->input->post('username',TRUE),ENT_QUOTES);
        $password=htmlspecialchars($this->input->post('password',TRUE),ENT_QUOTES);
        $pass=$this->input->post('password');

         $cek_admin=$this->login_model->auth_admin($username,$password);
        if($cek_admin->num_rows() > 0){ //jika login sebagai admin
						$data=$cek_admin->row_array();
        			$this->session->set_userdata('masuk',TRUE);
		            $this->session->set_userdata('akses','1');
		            $this->session->set_userdata('ses_id',$data['email']);
		            $this->session->set_userdata('ses_nama',$data['nama']);
		            redirect('admin/home');
        }

        $getdata = $this->login_model->getdata($username);
        $user = $this->login_model->getuser($getdata[0]->id_pegawai);
        $cek_dosen=$this->login_model->auth_dosen($username,$password);
        if($cek_dosen->num_rows() > 0){ //jika login sebagai dosen
						$data=$cek_dosen->row_array();
        			$this->session->set_userdata('masuk',TRUE);
		            $this->session->set_userdata('akses','2');
		            $this->session->set_userdata($user[0]);

		            // $getdata = $this->login_model->getdata($username);
              //   	$user = $this->login_model->getuser($getdata->id_pegawai);
              //   	$this->session->set_userdata('nama',$user['nama']);
		             redirect('dosen/home');


        }

				$cek_user=$this->login_model->auth_user($username,$password);
				       if($cek_user->num_rows() > 0){ //jika login sebagai peserta
				           $data=$cek_user->row_array();
				           if($data['status']=='1'){ //Akses status
				             $this->session->set_userdata('masuk',TRUE);
				               $this->session->set_userdata('akses','3');
											 $this->session->set_userdata('emp_id',$data['emp_id']);
											 $this->session->set_userdata('ses_id',$data['email']);
											 $this->session->set_userdata('ses_nama',$data['emp_name']);
				               redirect('non_civitas/home');
										 }
									 }
        //Jika Login Sebagai Mahasiswa lewat server eis
        $data = file_get_contents('http://eis.uinsby.ac.id/eis/login/'.$username.'/'.$pass);
        $obj = json_decode($data, true);
        if(!empty($obj[0]))
                {	$this->session->set_userdata('masuk',TRUE);
		            $this->session->set_userdata('akses','3');
                    $this->session->set_userdata($obj[0]);
                    redirect('user/home');
                }



        else{ //jika login sebagai mahasiswa
					$cek_mahasiswa=$this->login_model->auth_mahasiswa($username,$password);
					if($cek_mahasiswa->num_rows() > 0){
							$data=$cek_mahasiswa->row_array();
        					$this->session->set_userdata('masuk',TRUE);
							$this->session->set_userdata('akses','3');
							$this->session->set_userdata('ses_id',$data['id']);
							$this->session->set_userdata('ses_nim',$data['nim']);
							$this->session->set_userdata('ses_nama',$data['nama']);
							redirect('user/home');
					}
					else{  // jika username dan password tidak ditemukan atau salah
							$url=base_url();
							echo $this->session->set_flashdata('msg','Username Atau Password Salah !!');
							redirect('login');
					}
        }

    }

    function logout(){
        $this->session->sess_destroy();
        $url=base_url('');
        redirect($url);
    }

}
